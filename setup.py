# # setup script for py2exe to create the miniterm.exe
# # $Id: setup-miniterm-py2exe.py,v 1.1 2005-09-21 19:51:19 cliechti Exp $
#
# from distutils.core import setup
# import glob, sys, py2exe, os
#
# sys.path.insert(0, '..')
#
# sys.argv.extend("py2exe --bundle 1".split())
#
# import serial.tools.miniterm
#
# setup(
#     name = 'miniterm',
#     zipfile = None,
#     options = {"py2exe": {
#         'dll_excludes': [],
#         'includes': [
#                 'serial.urlhandler.protocol_hwgrep', 'serial.urlhandler.protocol_rfc2217',
#                 'serial.urlhandler.protocol_socket', 'serial.urlhandler.protocol_loop',],
#         'dist_dir': 'bin',
#         'excludes': ['serialjava', 'serialposix', 'serialcli'],
#         'compressed': 1,
#         }
#     },
#     console = [
#         #~ "miniterm.py",
#         serial.tools.miniterm.__file__
#     ],
# )

# ------------------------------------------------------------------------------
#                             how to create exe file
# change path to the folder of scripts, than run below command
# python setup.py py2exe

from distutils.core import setup
import py2exe
import os
py2exe_option={"py2exe":{"bundle_files":3,
                         "compressed": 1,
                         "optimize": 2,
                         }}

# icon = os.path.join(os.getcwd(), 'zr.ico')

setup(
    # console=['ZRSD-F528 UpgradeTool.py'],             # debug
    windows=[
        {
            'name':'UpgradeTool',
            'script':'timeConvert.py',      # release
            # 'icon_resources': [(1, "zr.ico")]
        }
    ],
#     console=['zrPackaging.py'],
#     windows=['zrPackaging.py'],
    options=py2exe_option,
    data_files=[(".\\",["zr.ico"])],
    # icon_resources=[(1,"zr.ico")],
    zipfile=None
)