import _winreg
import time


COMPILE_TIME = 1431043523
EXCEED_TIME = 1431043523
currentTime = time.time()

print "Compile time:", COMPILE_TIME
print "Exceed time:", EXCEED_TIME
print "Current time:", currentTime

key = _winreg.CreateKey(_winreg.HKEY_CURRENT_CONFIG, r"Software\zhongruisd")

# write to reg at first time or read from reg the first running timestamp
try:
    installTime, type = _winreg.QueryValueEx(key, "installTime")
    print "Install time:", installTime
except:
    _winreg.SetValueEx(key, "installTime", 0, _winreg.REG_DWORD, int(time.time()))
    print "First time!"

# get last execute time from reg
try:
    lastExcuTime, type = _winreg.QueryValueEx(key, "lastExcuTime")
    print "Last execute time:", lastExcuTime
except:
    pass

# judge
if currentTime < COMPILE_TIME or currentTime < installTime \
        or currentTime < lastExcuTime or currentTime > EXCEED_TIME:
    print "Exceed!!!!!"
    exit()
else:
    # record last execute time to reg
    _winreg.SetValueEx(key, "lastExcuTime", 0, _winreg.REG_DWORD, int(time.time()))
    installTime, type = _winreg.QueryValueEx(key, "installTime")
    lastExcuTime, type = _winreg.QueryValueEx(key, "lastExcuTime")
    print _winreg.EnumValue(key, 0)



