#-*- encoding=UTF-8 -*-
__author__ = 'Brain'
from Tkinter import *
import time
root = Tk()

def hello():
    pass

def about():
    pass

menubar = Menu(root)

#创建下拉菜单File，然后将其加入到顶级的菜单栏中
filemenu = Menu(menubar,tearoff=0)
filemenu.add_command(label="Open", command=hello)
filemenu.add_command(label="Save", command=hello)
filemenu.add_separator()
filemenu.add_command(label="Exit", command=root.quit)
menubar.add_cascade(label="File", menu=filemenu)

#创建另一个下拉菜单Edit
editmenu = Menu(menubar, tearoff=0)
editmenu.add_command(label="Cut", command=hello)
editmenu.add_command(label="Copy", command=hello)
editmenu.add_command(label="Paste", command=hello)
menubar.add_cascade(label="Edit",menu=editmenu)
#创建下拉菜单Help
helpmenu = Menu(menubar, tearoff=0)
helpmenu.add_command(label="About", command=about)
menubar.add_cascade(label="Help", menu=helpmenu)

#显示菜单
root.config(menu=menubar)
# root.minsize(200,200)
root.maxsize(500,500)
root.title("程序员的工具箱")
root.rowconfigure(5, weight = 4, pad = 4)
root.columnconfigure(10, weight = 4, pad = 4)
root.resizable(False, False)

def timeConvertSec2Date():
#     print "时间戳转为年月日"
    strInput = txtTimeInput.get()
#     print "Input:\r\n"
#     print strInput
    if strInput.isdigit():
        timeConvertLocal = time.strftime(ISOTIMEFORMAT, time.localtime(float(strInput)))
        timeConvertGMT = time.strftime(ISOTIMEFORMAT, time.gmtime(float(strInput)))
#         timeConvertGMT = str(timeConvertGMT) + " GMT"
#         print timeConvertGMT
#         print timeConvertLocal
        txtTimeOutputGMT.delete(0,END)
        txtTimeOutputGMT.insert(INSERT, timeConvertGMT)
        txtTimeOutputLocal.delete(0,END)
        txtTimeOutputLocal.insert(INSERT, timeConvertLocal)

def timeConvertDate2Sec():
#     print "年月日转为时间戳"
    strInput = txtDateInput.get()
    if len(strInput) == 19:
#         print "19 个字符"
        struct_time = time.strptime(strInput, ISOTIMEFORMAT)
#         print struct_time
        timeConvert = int(time.mktime(struct_time))
#         print timeConvert
#         timeConvert = str(timeConvert) + " GMT"
        txtDateOutputGMT.delete(0,END)
        txtDateOutputGMT.insert(INSERT, timeConvert)

# ===============显示当前时间==========================
ISOTIMEFORMAT='%Y-%m-%d %X'
# strTimeZone = time.strftime('%Z', time.localtime())
# print strTimeZone
timeZone = time.timezone
timeZone = timeZone / 3600.0
# print timeZone
timeNowStamp = int(time.time()) #获得当前时间戳
timeNowGMT = time.strftime(ISOTIMEFORMAT,time.gmtime(time.time())) #获得本地时间并进行格式
timeNowLocal = time.strftime(ISOTIMEFORMAT,time.localtime(time.time())) #获得本地时间并进行格式

labTime = Label(root,text = "当前时间：(您的时区是" + str(timeZone) + ")")
labTimeNowStamp = Label(root,text = "时间戳")
labTimeNowGMT = Label(root,text =  "GMT时间")
labTimeNowLocal = Label(root,text = "您的时间")

txtTimeNowStamp = Text(root,heigh = 1, width = 25)
txtTimeNowGMT   = Text(root,heigh = 1, width = 25)
txtTimeNowLocal = Text(root,heigh = 1, width = 25)
txtTimeNowStamp.insert(INSERT, timeNowStamp)
txtTimeNowGMT.insert(INSERT, timeNowGMT)
txtTimeNowLocal.insert(INSERT, timeNowLocal)

rowN = 1
labTime.grid(        row = rowN + 0, column = 0, columnspan = 2)
labTimeNowStamp.grid(row = rowN + 1, column = 0, sticky = 'e')
labTimeNowGMT.grid(  row = rowN + 2, column = 0, sticky = 'e')
labTimeNowLocal.grid(row = rowN + 3, column = 0, sticky = 'e')
txtTimeNowStamp.grid(row = rowN + 1, column = 1, sticky = 'w')
txtTimeNowGMT.grid(  row = rowN + 2, column = 1, sticky = 'w')
txtTimeNowLocal.grid(row = rowN + 3, column = 1, sticky = 'w')

# ===============输入时间戳计算日期==========================
labTimeConvert = Label(root,text = "输入时间戳转换为日期:")
labTimeInput = Label(root,text = "时间戳")
labTimeOutputGMT = Label(root,text = "GMT时间")
labTimeOutputLocal = Label(root,text = "本地时间")

txtTimeInput = Entry(root, width = 25)
txtTimeOutputGMT = Entry(root, width = 25)
txtTimeOutputLocal = Entry(root, width = 25)
btnCovertSec = Button(root,text = '转换',command = timeConvertSec2Date)

rowN = 5
labTimeConvert.grid(    row = rowN + 0, column = 0, columnspan = 2)
labTimeInput.grid(      row = rowN + 1, column = 0, sticky = 'e')
labTimeOutputGMT.grid(  row = rowN + 2, column = 0, sticky = 'e')
labTimeOutputLocal.grid(row = rowN + 3, column = 0, sticky = 'e')
txtTimeInput.grid(      row = rowN + 1, column = 1, sticky = 'w')
txtTimeOutputGMT.grid(  row = rowN + 2, column = 1, sticky = 'w')
txtTimeOutputLocal.grid(row = rowN + 3, column = 1, sticky = 'w')
btnCovertSec.grid(      row = rowN + 4, column = 0, columnspan = 2)

# ===============输入日期计算时间戳==========================
labDateConvert = Label(root,text = "输入日期转换为时间戳:")
labDateInput = Label(root,text = "日期")
labDateOutputGMT = Label(root,text = "GMT时间戳")

txtDateInput = Entry(root, width = 25)
txtDateOutputGMT = Entry(root, width = 25)
btnCovertDate = Button(root,text = '转换',command = timeConvertDate2Sec)

rowN = 11
labDateConvert.grid(    row = rowN + 0, column = 0, columnspan = 2)
labDateInput.grid(      row = rowN + 1, column = 0, sticky = 'e')
labDateOutputGMT.grid(  row = rowN + 2, column = 0, sticky = 'e')
txtDateInput.grid(      row = rowN + 1, column = 1, sticky = 'w')
txtDateOutputGMT.grid(  row = rowN + 2, column = 1, sticky = 'w')
btnCovertDate.grid(     row = rowN + 3, column = 0, columnspan = 2)

mainloop()