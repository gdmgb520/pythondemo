__author__ = 'Administrator'


gVarGlobal = 'I am module global'

class myClass():
    gVarClass = 'I am class global'

    def myFunc(self):
        global gVarGlobal           # this definition is needed
        locVarLocal = 'I am local'
        print locVarLocal
        print gVarGlobal
        print self.gVarClass
        print 'End'

if __name__=='__main__':
    obj = myClass()
    obj.myFunc()