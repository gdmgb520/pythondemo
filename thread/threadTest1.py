from Tkinter import *
from ttk import Button
import tkFileDialog   # get standard dialogs
import serialCOM


class Application(Frame):

    def say_hi(self):
        print "hi there, everyone!"

    def askopenfilename(self):
        """Returns an opened file in read mode.
        This time the dialog just returns a filename and the file is opened by your own code.
        """

        # get filename
        filename = tkFileDialog.askopenfilename()

        # open file on your own
        if filename:
            print filename
            self.strFileName.set(filename)
            # self.etrFile.insert(0, filename)
            return open(filename, 'r')

    def search(self):
        """ Search device
        Call scan_dev method of ZrDevice class
        Display the device info in text area
        :return:
        """
        # if not in search than start search
        if self.bSearchDev == False:
            print('Search devices')
            self.bSearchDev = True                  # set search state flag
            self.butSearch.config(state=DISABLED)   # disable Search button
            self.etrDev.delete(INSERT, END)         # clean device info text
            # self.etrDev.insert(INSERT, '')
            DevInfo = ZrObj.scan_dev()
            if (DevInfo != None):                   # if device found
                # self.etrDev.insert(INSERT, DevInfo)
                print 'Find device on port:' + ZrObj.serPort.portstr
                self.DevInfoStr.set(DevInfo)
            self.butSearch.config(state=NORMAL)     # enable Search button
            self.bSearchDev = False                 # clean search state flag
        else:
            pass


    def upgrade(self):
        """ Start upgrade

        :return:
        """
        print('Start upgrade')
        ZrObj.serPort.write("hello I am serial port")


    def createWidgets(self):
        # create frame and widget for file select
        self.frmSelectFile = LabelFrame(root, text='Choose upgrade file', height=40, width=585, bd=2)
        # use padx/pady to make margin outside and ipadx/ipady to make inside margin
        self.frmSelectFile.pack(side=TOP, anchor=NW, padx=10, pady=10, ipadx=10, ipady=10)

        # create frame for content
        self.frmContent = Frame(root, height=400, width=600, bd=2)
        # use padx/pady to make margin outside and ipadx/ipady to make inside margin
        self.frmContent.pack(side=TOP, anchor=NW, padx=10, pady=10)

        # create frame for running info
        self.frmLog = LabelFrame(self.frmContent, text='Communication info',  height=400, width=390, bd=2)
        # it's strange the ipady have no effect
        self.frmLog.pack(side=LEFT, ipady=10)

        # create frame for options and buttons
        self.frmOption = Frame(self.frmContent, height=400, width=200, bd=2)
        self.frmOption.pack(side=RIGHT, anchor=NW)

        # create frame for device search
        self.frmDevice = LabelFrame(self.frmOption, text='Device', height=150, width=200, bd=2)
        self.frmDevice.pack(side=TOP, anchor=NW, padx=5)

        # create frame for upgrade
        self.frmUpgrade = LabelFrame(self.frmOption, text='Upgrade', height=150, width=200, bd=2)
        self.frmUpgrade.pack(side=TOP, anchor=NW, padx=5)

        # --- start create widget of file_select
        self.frmSelectFile.pack_propagate(0)
        self.lblFile = Label(self.frmSelectFile, text="Select file:", padx=1)
        self.lblFile.pack(side=LEFT)

        self.etrFile = Entry(self.frmSelectFile, textvariable=self.strFileName, bd=1, width=60)
        self.etrFile.pack(side=LEFT, padx=10)

        self.butBrowse = Button(self.frmSelectFile, text='Browse', command=self.askopenfilename)
        self.butBrowse.pack(side=LEFT)
        # --- end create widget of file_select

        # --- start create widget for Log_info
        self.frmLog.pack_propagate(0)
        self.scbInfo = Scrollbar(self.frmLog)
        self.scbInfo.pack(side=RIGHT, fill=Y)
        self.txtInfo = Text(self.frmLog, yscrollcommand=self.scbInfo.set)
        self.txtInfo.pack(side=LEFT, fill=Y)
        # --- end create widget

        # --- start create widget for device search
        # tell frame not to let its children control its size
        self.frmDevice.pack_propagate(0)
        self.lblDev = Label(self.frmDevice, text='Device inof:')
        self.lblDev.pack(side=TOP, anchor=NW)
        self.etrDev = Entry(self.frmDevice, width=26, textvariable=self.DevInfoStr)
        self.etrDev.pack(side=TOP, anchor=NW, pady=5)
        self.butSearch = Button(self.frmDevice, textvariable=self.strSearchBut, width=10, command=self.search)
        self.butSearch.pack(side=TOP, anchor=SE, padx=8)
        # --- end create widget

        # --- start create widget for upgrade
        self.frmUpgrade.pack_propagate(0)
        self.butUpgrade = Button(self.frmUpgrade, text='Start', width=10, command=self.upgrade)
        self.butUpgrade.pack(side=BOTTOM, anchor=E, padx=8, pady=8)
        # --- end create widget

        # --- start create widget for menu
        menubar = Menu(root)

        # Create "File" menu and add it to menubar
        filemenu = Menu(menubar,tearoff=0)
        filemenu.add_command(label="Open", command=self.hello)
        filemenu.add_command(label="Save", command=self.hello)
        filemenu.add_separator()
        filemenu.add_command(label="Exit", command=root.quit)
        menubar.add_cascade(label="File", menu=filemenu)

        # Create "Edit" menu and add it to menubar
        editmenu = Menu(menubar, tearoff=0)
        editmenu.add_command(label="Cut", command=self.hello)
        editmenu.add_command(label="Copy", command=self.hello)
        editmenu.add_command(label="Paste", command=self.hello)
        menubar.add_cascade(label="Edit",menu=editmenu)

        # Create "Help" menu and add it to menubar
        helpmenu = Menu(menubar, tearoff=0)
        helpmenu.add_command(label="About", command=self.about)
        menubar.add_cascade(label="Help", menu=helpmenu)

        #Show menu
        root.config(menu=menubar)
        # --- end create widget

        # --- start create widget
        # --- end create widget

    def hello(self):
        print('hello')
        pass

    def about(self):
        print('ZRSD')
        pass

    def __init__(self, master=None):
        self.bSearchDev = False
        self.strSearchBut = StringVar()
        self.strSearchBut.set('Search')
        self.strFileName = StringVar()
        self.strFileName.set('')
        self.DevInfoStr = StringVar()

        Frame.__init__(self, master)
        self.pack(expand=YES, fill=BOTH)
        # set the window unresizable
        root.resizable(width=False, height=False)
        root.title('Upgrade Tool')
        root.iconbitmap('zr.ico')
        self.createWidgets()



root = Tk()
app = Application(master=root)
ZrObj = serialCOM.ZrDevice()
app.mainloop()

