# coding=utf-8

__author__ = 'Administrator'

import threading
import urllib2
import socket


class FetchUrls(threading.Thread):
    """
    下载URL内容的线程
    """

    def __init__(self, urls, output, lock):
        """
        构造器

        @param urls 需要下载的URL列表
        @param output 写URL内容的输出文件
        """
        threading.Thread.__init__(self)
        self.urls = urls
        self.output = output
        self.lock = lock  #传入的lock对象

    def run(self):
        """
        实现父类Thread的run方法，打开URL，并且一个一个的下载URL的内容
        """
        while self.urls:
            url = self.urls.pop()
            req = urllib2.Request(url)
            try:
                d = urllib2.urlopen(req)
            except urllib2.URLError, e:
                print 'URL %s failed: %s' % (url, e.reason)
            self.lock.acquire()
            print 'lock acquired by %s' % self.name
            self.output.write(d.read())
            print 'write done by %s' % self.name
            print 'lock released by %s' % self.name
            self.lock.release()
            print 'URL %s fetched by %s' % (url, self.name)


def main():
    # URL列表1
    urls1 = ['http://www.baidu.com', 'http://www.163.com']
    # URL列表2
    urls2 = ['http://www.elecbench.com', 'http://www.douban.com']
    f = open('output.txt', 'w+')
    lock = threading.Lock()
    t1 = FetchUrls(urls1, f, lock)
    t2 = FetchUrls(urls2, f, lock)
    t1.start()
    t2.start()
    t1.join()
    t2.join()
    f.close()


if __name__ == '__main__':
    main()
