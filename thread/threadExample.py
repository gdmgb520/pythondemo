# #!/usr/bin/python
#
# import threading
# import time
#
# exitFlag = 0
#
# class myThread (threading.Thread):
#     def __init__(self, threadID, name, counter):
#         threading.Thread.__init__(self)
#         self.threadID = threadID
#         self.name = name
#         self.counter = counter
#     def run(self):
#         print "Starting " + self.name
#         print_time(self.name, self.counter, 5)
#         print "Exiting " + self.name
#
# def print_time(threadName, delay, counter):
#     while counter:
#         if exitFlag:
#             thread.exit()
#         time.sleep(delay)
#         print "%s: %s %d" % (threadName, time.ctime(time.time()), counter)
#         counter -= 1
#     print "%s: end while" % (threadName)
#
# # Create new threads
# thread1 = myThread(1, "Thread-1", 1)
# thread2 = myThread(2, "Thread-2", 2)
#
# # Start new Threads
# thread1.start()
# thread2.start()
#
# print "Exiting Main Thread"


















#!/usr/bin/python

import threading
import time
import Queue
from Tkinter import *

shared_queue = Queue.Queue()

class myThread (threading.Thread):
    def __init__(self, threadID, name, counter):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.counter = counter
    def run(self):
        print "Starting " + self.name
        # Get lock to synchronize threads
        # threadLock.acquire()
        print_time(self.name, self.counter, 3)
        # Free lock to release next thread
        # threadLock.release()

def print_time(threadName, delay, counter):
    while counter:
        if (counter == 2):
            threadLock.acquire()
        time.sleep(delay)
        print "%s: %s" % (threadName, time.ctime(time.time()))
        counter -= 1
        if (counter == 0):
            threadLock.release()
        if threadName == "Thread-1":
            print 'put data'
            shared_queue.put('Thread1 data')
            print shared_queue.qsize()
        if threadName == "Thread-2":
            print 'put data 2'
            shared_queue.put('Thread2 data')
            print shared_queue.qsize()

threadLock = threading.Lock()
threads = []


if __name__ == '__main__':
    root = Tk()

    # Create new threads
    thread1 = myThread(1, "Thread-1", 1)
    thread2 = myThread(2, "Thread-2", 2)

    # Start new Threads
    thread1.start()
    thread2.start()

    # Add threads to thread list
    threads.append(thread1)
    threads.append(thread2)

    def queue_consumer():
        # handle events
        # func, args = shared_queue.get(block=False)
        # func(*args)
        print 'call consumer'
        if shared_queue.empty() != True:
            strData = shared_queue.get(block=False)
            if strData:
                print strData
        # root.after(50, queue_consumer)
        root.after(50, queue_consumer)

    queue_consumer()

    print shared_queue.qsize()

    # Wait for all threads to complete
    for t in threads:
        t.join()
    print "Exiting Main Thread"