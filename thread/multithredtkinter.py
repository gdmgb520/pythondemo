__author__ = 'Administrator'

from Tkinter import *
from ttk import *

import Queue

shared_queue = Queue.Queue()
# (func_name, ())

if __name__ == '__main__':
    root = Tk()

    def queue_consumer():
        # handle events
        func, args = shared_queue.get(block=False)
        func(*args)
        root.after(50, queue_consumer)

    queue_consumer()

    root.mainloop()