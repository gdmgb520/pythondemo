#! /usr/bin/env python
"""\
Scan for serial ports.

Part of pySerial (http://pyserial.sf.net)
(C) 2002-2003 <cliechti@gmx.net>

The scan function of this module tries to open each port number
from 0 to 255 and it builds a list of those ports where this was
successful.
"""

import serial
import binascii

def scan():
    """scan for available ports. return a list of tuples (num, name)"""
    available = []
    for i in range(256):
        try:
            # open serial port as "9600,8,N,1",timeout:1s
            serPort = serial.Serial(i, 9600, timeout=1)
            serPort.write(serial.to_bytes([0xF5, 0xFA, 0xE1, 0xA1, 0x00, 0x09, 'Z', 'H', 'O', 'N', 'G', 'R', 'U', 'I', 0xFF]))
            # ack = []
            ack = serPort.read(18)
            hexstr = binascii.b2a_hex(ack)
            # print '%x' %(ack[0])
            print hexstr
            text = ack

            text = ''.join([(c >= ' ') and c or '<%d>' % ord(c)  for c in text])
            print text
            # print s
            # print s.port
            # print s.portstr
            # print s.baudrate
            available.append( (i, serPort.portstr))
            serPort.close()   # explicit close 'cause of delayed GC in java
        except serial.SerialException:
            pass
    print available
    print available[0]
    n,serPort = available[0]
    print n,serPort
    return available

if __name__=='__main__':
    print "Found ports:"
    scan()
    # for n,s in scan():
    #     print "(%d) %s" % (n,s)
