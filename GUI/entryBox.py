#-*- coding: utf-8 -*-

import win32gui as gui
import windowControl as wind
# import sin32process as p

hwnd = gui.FindWindowEx(None,None,"MainWindow","Greenshot image editor")

print hwnd




# coding=utf-8
__author__ = 'Administrator'

# ---------------------------------------
# --------------- history ---------------
# ---------------------------------------

# 24/4/2015
# refactor

from Tkinter import *
from ttk import Button
import tkFileDialog   # get standard dialogs
import tkMessageBox
import struct
import threading


class AboutFrame(Frame):
    """
    For about frame
    """
    def __init__(self, parent):
        Frame.__init__(self, parent, background="white")
        self.parent = parent
        self.initUI()

    def initUI(self):
        self.parent.title("About")
        self.parent.iconbitmap('zr.ico')
        self.parent.resizable(width=False, height=False)
        self.pack(fill=BOTH, expand=1)
        self.createWidgets()

    def createWidgets(self):
        self.lblCmp = Label(self.parent, text="中瑞盛大科技有限公司", padx=1)
        self.lblCmp.pack(side=TOP)
        self.lblPhone = Label(self.parent, text="Phone: +86-0755-29660689\r        +86-15989310185", padx=1)
        self.lblPhone.pack(side=TOP)
        self.lblEmail = Label(self.parent, text="Email: winfei001@126.com", padx=1)
        self.lblEmail.pack(side=TOP)
        self.lblWeb = Label(self.parent, text="WebSite: www.winfei.com", padx=1)
        self.lblWeb.pack(side=TOP)



BIT0 = 0X01
BIT1 = 0X02
BIT2 = 0X04
BIT3 = 0X08
BIT4 = 0X10
BIT5 = 0X20
BIT6 = 0X40
BIT7 = 0X80

# identity code to store in file for recognize
listIDCode = ['\x05', '\xfe', '\x88', '\x65', '\x94', '\x9f', '\xe6',
              '\x89', '\xfe', '\x83', '\x76', '\xa6', '\xf9', '\x9e',
              '\x72', '\xfa', '\x96', '\x97', '\xac', '\x59', '\x8f',
              '\x31', '\x99', '\x87', '\x88', '\xb4', '\xf5', '\xc7']


class Application(Frame):

    key = []            # encode key
    # StartID = 0         # startID
    IDList = []

    def createWidgets(self):
        # create frame for content
        self.frmContent = LabelFrame(root, text='Input KEY', height=80, width=555, bd=2)
        # use padx/pady to make margin outside and ipadx/ipady to make inside margin
        self.frmContent.pack(side=TOP, anchor=NW, padx=10, pady=10, ipadx=10, ipady=10)

        # --- start create widget for upgrade
        self.frmContent.pack_propagate(0)
        self.lblKey = Label(self.frmContent, text='Key:')
        self.lblKey.pack(side=LEFT)
        self.etrKey = Entry(self.frmContent, bd=1, width=20)
        self.etrKey.pack(side=LEFT, padx=10)

        self.lblStrartID = Label(self.frmContent, text=' StartID:')
        self.lblStrartID.pack(side=LEFT)

        self.etrStrartID = Entry(self.frmContent, bd=1, width=18, textvariable=self.strStartID)
        self.etrStrartID.pack(side=LEFT, padx=10)

        self.lblCurID = Label(self.frmContent, text=' CurrentID:')
        self.lblCurID.pack(side=LEFT)
        self.etrCurID = Entry(self.frmContent, bd=1, width=18, textvariable=self.strCurrentID)
        self.etrCurID.pack(side=LEFT)
        self.partiallyEnc = IntVar()

        # create frame for content
        self.frmButton = LabelFrame(root, text='Action', height=80, width=585, bd=2)
        # use padx/pady to make margin outside and ipadx/ipady to make inside margin
        self.frmButton.pack(side=RIGHT, anchor=NW, padx=10, pady=10, ipadx=10, ipady=10)

        self.butAgain = Button(self.frmButton, text='Again', width=10, command=self.WriteCurrentID)
        self.butAgain.pack(side=LEFT, padx=10)
        self.butNext = Button(self.frmButton, text='Next', width=10, command=self.WriteNextID)
        self.butNext.pack(side=LEFT, padx=10)
        # --- end create widget

        # --- start create widget for menu
        menubar = Menu(root)

        # Create "File" menu and add it to menubar
        filemenu = Menu(menubar,tearoff=0)
        filemenu.add_command(label="Open", command=None)
        filemenu.add_separator()
        filemenu.add_command(label="Exit", command=root.quit)
        menubar.add_cascade(label="File", menu=filemenu)

        # Create "Help" menu and add it to menubar
        helpmenu = Menu(menubar, tearoff=0)
        helpmenu.add_command(label="About", command=self.about)
        menubar.add_cascade(label="Help", menu=helpmenu)

        #Show menu
        root.config(menu=menubar)
        # --- end create widget

    def getInput(self):
        """
        get input of encode key, startID
        :return:
        """
        # get encode key
        key = self.etrKey.get()
        if len(key) != 16:
            tkMessageBox.showinfo("Error", "KEY is wrong!")
            return False
        self.key = []
        for i in range(0, len(key), 2):
            self.key.append(int(key[i:i+2], 16))

        # get StartID
        self.StartID = int(self.etrStrartID.get())
        if self.StartID > 4294967295:
            tkMessageBox.showinfo("Error", "StartID out of range!")
            return False

        self.strStartID.set('2222')
        return True

    def convertInt2HexStr(self,list):
        lRes = []
        for d in list:
            # convert integer d to format 0xXX, add string XX to list lRes
            lRes.append('%02X'%(d))
        return lRes

    def WriteCurrentID(self):
        print "WriteCurrentID"
        # CodeList = ['36', '03', '0B', '0C']
        wind.WriteFlash(self.IDList)


    def WriteNextID(self):
        print "WriteNextID"
        self.getInput()
        # convert StartID
        self.IDList = []
        self.IDList.append(self.StartID & 0xFF)
        self.IDList.append((self.StartID >> 8) & 0xFF)
        self.IDList.append((self.StartID >> 16) & 0xFF)
        self.IDList.append((self.StartID >> 24) & 0xFF)
        self.IDList.reverse()
        print "IDList:", self.IDList
        self.IDList = self.encode(self.IDList)
        self.IDList = self.convertInt2HexStr(self.IDList)
        print "IDListNew:", self.IDList

        # CodeList = ['37', '03', '0B', '0C']
        # wind.WriteFlash(self.IDList)

        # self.etrCurID
        threading.Thread(target=wind.WriteFlash, args=(self.IDList, )).start()

    def encode(self, lData):
        temp2 = 0x0c

        # self.key = [0xef,0xcd,0xab,0x89,0x67,0x45,0x23,0x01]
        ID1,ID2,ID3,ID4,ID5,ID6,ID7,ID8 = self.key
        c5, c6, c7, c8 =lData

        while temp2 > 0:
            temp1 = 0x30
            while temp1 > 0:
                if temp2 != 1:
                    if c6 & BIT4 == 0:
                        temp3 = 0x01
                    else:
                        temp3 = 0x10
                    if c7 & BIT1 == 0:
                        pass
                    else:
                        temp3 <<= 2

                    if c8 & BIT1 == 0:
                        if c5 & BIT7 == 0:
                            if c5 & BIT2 == 0:
                                w = 0x2E
                            else:
                                w = 0x74
                        else:
                            if c5 & BIT2 == 0:
                                w = 0x5c
                            else:
                                w = 0x3a
                    else: # ==1
                        temp3 <<= 1
                        if c5 & BIT7 == 0:
                            if c5 & BIT2 == 0:
                                w = 0x2E
                            else:
                                w = 0x74
                        else:
                            if c5 & BIT2 == 0:
                                w = 0x5c
                            else:
                                w = 0x3a


                    if temp3 & w == 0:
                        temp3 = 0x00
                    else:
                        temp3 = 0x80
                    # left shift with C
                    if temp3 & BIT7 == BIT7:
                        cBit = 1
                    else:
                        cBit = 0
                    temp3 <<= 1
                    if temp3 & BIT7 == BIT7:
                        cBit2 = 1
                    else:
                        cBit2 = 0
                    temp3 <<= 1
                    temp3 |= cBit

                    temp3 ^= c8
                    temp3 ^= c6
                    temp3 ^= ID1

                    tmp = (temp3<<32) | (c5<<24) | (c6<<16) | (c7<<8) | c8
                    tmp >>= 1
                    tmp |= (cBit2<<32)
                    c8 = tmp & 0xff
                    c7 = (tmp >> 8) & 0xff
                    c6 = (tmp >> 16) & 0xff
                    c5 = (tmp >> 24) & 0xff
                    temp3 = (tmp >> 32) & 0xff
                else:
                    pass

                if ID1 & BIT0 == 1: # High bit fill 1
                    tmp = (ID8<<56) |(ID7<<48) | (ID6<<40) | (ID5<<32) | (ID4<<24) | (ID3<<16) | (ID2<<8) | ID1
                    tmp >>= 1
                    tmp |= 0x8000000000000000
                    ID1 = tmp & 0xff
                    ID2 = (tmp >> 8) & 0xff
                    ID3 = (tmp >> 16) & 0xff
                    ID4 = (tmp >> 24) & 0xff
                    ID5 = (tmp >> 32) & 0xff
                    ID6 = (tmp >> 40) & 0xff
                    ID7 = (tmp >> 48) & 0xff
                    ID8 = (tmp >> 56) & 0xff
                else:
                    tmp = (ID8<<56) |(ID7<<48) | (ID6<<40) | (ID5<<32) | (ID4<<24) | (ID3<<16) | (ID2<<8) | ID1
                    tmp >>= 1
                    ID1 = tmp & 0xff
                    ID2 = (tmp >> 8) & 0xff
                    ID3 = (tmp >> 16) & 0xff
                    ID4 = (tmp >> 24) & 0xff
                    ID5 = (tmp >> 32) & 0xff
                    ID6 = (tmp >> 40) & 0xff
                    ID7 = (tmp >> 48) & 0xff
                    ID8 = (tmp >> 56) & 0xff
                temp1 -= 1
            temp2 -= 1

        li = []
        li.append(c5)
        li.append(c6)
        li.append(c7)
        li.append(c8)
        # print "Finish!", hex(li[0]),hex(li[1]),hex(li[2]),hex(li[3])
        return li


    def about(self):
        """
        create an about window
        :return:
        """
        self.newWindow = Toplevel(self.master)
        self.app = AboutFrame(self.newWindow)
        pass

    def __init__(self, master=None):
        self.strStartID = StringVar()
        self.strStartID.set('')
        self.strCurrentID = StringVar()
        self.strCurrentID.set('')
        Frame.__init__(self, master)
        self.pack(expand=YES, fill=BOTH)
        # set the window unresizable
        root.resizable(width=False, height=False)
        root.title('ZRSD-F528 Packaging Tool')
        root.iconbitmap('zr.ico')
        self.createWidgets()

    def __exit__(self):
        pass

if __name__=='__main__':
    root = Tk()
    app = Application(master=root)
    app.mainloop()