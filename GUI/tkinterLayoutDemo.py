#coding=UTF-8
# __author__ = '极致'

# import Tkinter, Tkconstants, tkFileDialog
#
# class TkFileDialogExample(Tkinter.Frame):
#
#     def __init__(self, root):
#
#         Tkinter.Frame.__init__(self, root)
#
#         # options for buttons
#         button_opt = {'fill': Tkconstants.BOTH, 'padx': 5, 'pady': 5}
#
#         # define buttons
#         Tkinter.Button(self, text='askopenfile', command=self.askopenfile).pack(**button_opt)
#         Tkinter.Button(self, text='askopenfilename', command=self.askopenfilename).pack(**button_opt)
#         Tkinter.Button(self, text='asksaveasfile', command=self.asksaveasfile).pack(**button_opt)
#         Tkinter.Button(self, text='asksaveasfilename', command=self.asksaveasfilename).pack(**button_opt)
#         Tkinter.Button(self, text='askdirectory', command=self.askdirectory).pack(**button_opt)
#
#         # define options for opening or saving a file
#         self.file_opt = options = {}
#         options['defaultextension'] = '.txt'
#         options['filetypes'] = [('all files', '.*'), ('text files', '.txt')]
#         options['initialdir'] = 'C:\\'
#         options['initialfile'] = 'myfile.txt'
#         options['parent'] = root
#         options['title'] = 'This is a title'
#
#         # This is only available on the Macintosh, and only when Navigation Services are installed.
#         #options['message'] = 'message'
#
#         # if you use the multiple file version of the module functions this option is set automatically.
#         #options['multiple'] = 1
#
#         # defining options for opening a directory
#         self.dir_opt = options = {}
#         options['initialdir'] = 'C:\\'
#         options['mustexist'] = False
#         options['parent'] = root
#         options['title'] = 'This is a title'
#
#     def askopenfile(self):
#
#         """Returns an opened file in read mode."""
#
#         return tkFileDialog.askopenfile(mode='r', **self.file_opt)
#
#     def askopenfilename(self):
#
#         """Returns an opened file in read mode.
#         This time the dialog just returns a filename and the file is opened by your own code.
#         """
#
#         # get filename
#         filename = tkFileDialog.askopenfilename(**self.file_opt)
#
#         # open file on your own
#         if filename:
#             print filename
#             return open(filename, 'r')
#
#     def asksaveasfile(self):
#
#         """Returns an opened file in write mode."""
#
#         return tkFileDialog.asksaveasfile(mode='w', **self.file_opt)
#
#     def asksaveasfilename(self):
#
#         """Returns an opened file in write mode.
#         This time the dialog just returns a filename and the file is opened by your own code.
#         """
#
#         # get filename
#         filename = tkFileDialog.asksaveasfilename(**self.file_opt)
#
#         # open file on your own
#         if filename:
#             return open(filename, 'w')
#
#     def askdirectory(self):
#
#         """Returns a selected directoryname."""
#
#         return tkFileDialog.askdirectory(**self.dir_opt)
#
# if __name__ == '__main__':
#     root = Tkinter.Tk()
#     TkFileDialogExample(root).pack()
#     root.mainloop()









from Tkinter import *
import tkFileDialog   # get standard dialogs


class Application(Frame):

    def say_hi(self):
        print "hi there, everyone!"

    def askopenfilename(self):
        """Returns an opened file in read mode.
        This time the dialog just returns a filename and the file is opened by your own code.
        """

        # get filename
        filename = tkFileDialog.askopenfilename()

        # open file on your own
        if filename:
            print filename
            self.strFileName.set(filename)
            # self.etrFile.insert(0, filename)
            return open(filename, 'r')

    def search(self):
        """ Search device

        :return:
        """
        # if not in search than start search
        if self.bSearchDev == False:
            self.bSearchDev = True
            self.strSearchBut.set('Cancel')
            print('Search devices')
        else:
            self.bSearchDev = False
            self.strSearchBut.set('Search')
            print('Cancel search')



    def upgrade(self):
        """ Start upgrade

        :return:
        """
        print('Start upgrade')


    def createWidgets(self):
        # create frame and widget for file select
        self.frmSelectFile = LabelFrame(root, text='Choose upgrade file', height=40, width=585, bg='blue', bd=2)
        self.frmSelectFile.pack(side=TOP, anchor=NW, padx=10, ipadx=10, ipady=10)

        # create frame for content
        self.frmContent = Frame(root, height=400, width=600, bg='red', bd=2)
        self.frmContent.pack(side=TOP, anchor=NW, padx=10)

        # create frame for running info
        self.frmLog = LabelFrame(self.frmContent, text='Communication info',  height=400, width=390, bg='yellow', bd=2)
        self.frmLog.pack(side=LEFT, ipadx=1, ipady=10)

        # create frame for options and buttons
        self.frmOption = Frame(self.frmContent, height=400, width=200, bg='orange', bd=2)
        self.frmOption.pack(side=RIGHT, anchor=NW)

        # create frame for device search
        self.frmDevice = LabelFrame(self.frmOption, text='Device', height=150, width=200, bg='green', bd=2)
        self.frmDevice.pack(side=TOP, anchor=NW, padx=5)

        # create frame for upgrade
        self.frmUpgrade = LabelFrame(self.frmOption, text='Upgrade', height=150, width=200, bg='white', bd=2)
        self.frmUpgrade.pack(side=TOP, anchor=NW, padx=5)

        # --- start create widget of file_select
        self.frmSelectFile.pack_propagate(0)
        self.lblFile = Label(self.frmSelectFile, text="Select file:", padx=1)
        self.lblFile.pack(side=LEFT)

        self.etrFile = Entry(self.frmSelectFile, textvariable=self.strFileName, bd=1, width=60)
        self.etrFile.pack(side=LEFT, padx=10)

        self.butBrowse = Button(self.frmSelectFile, text='Browse', command=self.askopenfilename)
        self.butBrowse.pack(side=LEFT)
        # --- end create widget of file_select

        # --- start create widget for Log_info
        self.frmLog.pack_propagate(0)
        self.scbInfo = Scrollbar(self.frmLog)
        self.scbInfo.pack(side=RIGHT, fill=Y)
        self.txtInfo = Text(self.frmLog, yscrollcommand=self.scbInfo.set)
        self.txtInfo.pack(side=LEFT, fill=Y)
        # --- end create widget

        # --- start create widget for device search
        # tell frame not to let its children control its size
        self.frmDevice.pack_propagate(0)
        self.lblDev = Label(self.frmDevice, text='Device inof:')
        self.lblDev.pack(side=TOP, anchor=NW)
        self.etrDev = Entry(self.frmDevice, width=26)
        self.etrDev.pack(side=TOP, anchor=NW, pady=5)
        self.butSearch = Button(self.frmDevice, textvariable=self.strSearchBut, width=10, command=self.search)
        self.butSearch.pack(side=TOP, anchor=SE, padx=8)
        # --- end create widget

        # --- start create widget for upgrade
        self.frmUpgrade.pack_propagate(0)
        self.butUpgrade = Button(self.frmUpgrade, text='Start', width=10, command=self.upgrade)
        self.butUpgrade.pack(side=BOTTOM, anchor=E, padx=8, pady=8)
        # --- end create widget



        # --- start create widget
        menubar = Menu(root)

        # Create "File" menu and add it to menubar
        filemenu = Menu(menubar,tearoff=0)
        filemenu.add_command(label="Open", command=self.hello)
        filemenu.add_command(label="Save", command=self.hello)
        filemenu.add_separator()
        filemenu.add_command(label="Exit", command=root.quit)
        menubar.add_cascade(label="File", menu=filemenu)

        # Create "Edit" menu and add it to menubar
        editmenu = Menu(menubar, tearoff=0)
        editmenu.add_command(label="Cut", command=self.hello)
        editmenu.add_command(label="Copy", command=self.hello)
        editmenu.add_command(label="Paste", command=self.hello)
        menubar.add_cascade(label="Edit",menu=editmenu)

        # Create "Help" menu and add it to menubar
        helpmenu = Menu(menubar, tearoff=0)
        helpmenu.add_command(label="About", command=self.about)
        menubar.add_cascade(label="Help", menu=helpmenu)

        #Show menu
        root.config(menu=menubar)
        # --- end create widget

    def hello(self):
        print('hello')
        pass

    def about(self):
        print('ZRSD')
        pass

    def __init__(self, master=None):
        self.bSearchDev = False
        self.strSearchBut = StringVar()
        self.strSearchBut.set('Search')
        self.strFileName = StringVar()
        self.strFileName.set('')

        Frame.__init__(self, master)
        self.pack(expand=YES, fill=BOTH)
        # set the window size
        # root.geometry('580x540')
        # set the window unresizable
        root.resizable(width=False, height=False)
        self.createWidgets()

root = Tk()
app = Application(master=root)
app.mainloop()







