import logging
logging.basicConfig(level=logging.INFO)

def foo(s):
    n = int(s)
    logging.error('n = %d' % n)
    return 10 / n

def bar(s):
    return foo(s) * 2

def main():
    try:
        bar('0')
    except StandardError, e:
        logging.exception(e)

if __name__ == '__main__':
    main()
    print 'end'