__author__ = 'Administrator'


class ZrFile():
    fObj = None                     # file object
    fLen = 0                        # file lenth

    def __init__(self, fName):
        """
        open the file and create the file object
        :param fName: the data file need to read
        :return:
        file object to fObj, and file lenth to fLen
        """
        global fObj
        global fLen
        try:
            fObj = open(fName, 'rb')
            try:
                fLen = len(fObj.read())
                print 'file size %d' % (fLen)
            finally:
                # fObj.close()
                pass
        except IOError:
            print "IOError when open file: %s" % (fName)
            pass

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        release resource when exit
        :param exc_type:
        :param exc_val:
        :param exc_tb:
        :return:
        """
        fObj.close()

    # def get_file_size(self, fName):
    #     '''
    #     get file size
    #     count and return total bytes of the file.
    #     '''
    #     try:
    #         fObj = open(fName,'rb')
    #         try:
    #             allData = fObj.read()
    #         finally:
    #             fObj.close()
    #     except IOError:
    #         pass
    #     return len(allData)

    def get_data(self, size):
        """
        read data from file object
        :param fName:
        :param size: data size
        :return: specified size of file data
        """
        global fObj

        if fObj != None :
            try:
                allData = fObj.read(size)
            finally:
                # fObj.close()
                pass
        return allData

    def get_list(self):
        a = 2
        b = 1
        c = 'abc'
        li = []
        li.append(a)
        li.append(b)
        li.append(c)
        return li

if __name__=='__main__':
    zrfile = ZrFile("2410boot.bin")
    # print zrfile.get_file_size("2410boot.bin")
    print zrfile.get_data(5)
    print zrfile.get_list()