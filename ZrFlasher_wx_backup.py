#!/usr/bin/python
# -*- coding: cp936 -*-
import wx


# crate a class named Example, it inherited from class wx.Frame
class GbFrame(wx.Frame):
    # crate the initial structure function of class Example
    def __init__(self, parent):
        super(GbFrame, self).__init__(parent)

        self.init_ui()
        self.BindEvent()
        self.Show()

    def init_ui(self):
        # ---------------------------  menu bar  ---------------------------
        # 1.create menubar object
        menubar = wx.MenuBar()
        # 2.create menu object
        fileMenu = wx.Menu()
        editMenu = wx.Menu()
        helpMenu = wx.Menu()
        # 3.create a menu item by Append() method, append menu item to menu object
        # first parameter is the id of menu item.
        # second parameter is the name of menu item
        # help string displayed on statusbar
        # the return value is used for bind event
        self.fitem = fileMenu.Append(wx.ID_EXIT, 'Quit', 'Quit application')
        # 4.append menu object to menubar object
        menubar.Append(fileMenu, '&File')
        menubar.Append(editMenu, '&Edit')
        menubar.Append(helpMenu, '&Help')
        # 5.use wx.Frame SetMenuBar() method to sets up the menubar
        self.SetMenuBar(menubar)

        # ---------------------------  create container  ---------------------------
        panel = wx.Panel(self, -1)
        font = wx.SystemSettings_GetFont(wx.SYS_SYSTEM_FONT)
        # font.SetPointSize(9)
        vbox = wx.BoxSizer(wx.VERTICAL)

        # ----------------------------  add select file  -------------------------------
        hbox1 = wx.BoxSizer(wx.HORIZONTAL)
        st1 = wx.StaticText(panel, label='请选择升级文件：')
        vbox.Add(hbox1, flag=wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP, border=100)
        vbox.Add((-1, 10))  # add margin
        hbox2 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_filePicker1 = wx.FilePickerCtrl(panel, wx.ID_ANY, wx.EmptyString, u"Select a file", u"*.*")
        # hbox2.Add(st1, flag=wx.RIGHT, border=8)
        hbox2.Add(self.m_filePicker1, proportion=1)
        vbox.Add(hbox2, flag=wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP, border=10)
        vbox.Add((-1, 10))  # add margin



        # ---------------------------  add Start/Close button  -------------------------
        hbox3 = wx.BoxSizer(wx.HORIZONTAL)
        self.btnStart = wx.Button(panel, label='开始升级', size=(70, 30))
        hbox3.Add(self.btnStart, flag=wx.ALIGN_RIGHT)
        # self.btnClose = wx.Button(panel, label='', size=(70, 30))
        # hbox3.Add(self.btnClose, flag=wx.LEFT|wx.BOTTOM, border=5)
        vbox.Add(hbox3, flag=wx.EXPAND | wx.ALL, border=10)
        vbox.Add((-1, 50))  # add margin

        # hboxContent = wx.BoxSizer(wx.HORIZONTAL)
        # self.txtLgo = wx.TextCtrl(self, wx.ID_ANY, 'test', style=wx.TE_MULTILINE)
        # self.btnS = wx.Button(self,)

        # ---------------------------  add Start/Close button  -------------------------
        # hbox5 = wx.BoxSizer(wx.HORIZONTAL)
        # self.btn1 = wx.Button(panel, label='btn1', size=(70, 30))
        # self.btn2 = wx.Button(panel, label='btn2', size=(70, 30))
        # self.btn3 = wx.Button(panel, label='btn3', size=(70, 30))
        # self.txt = wx.TextCtrl(self, wx.ID_ANY, 'aa', style=wx.TE_MULTILINE)
        # hbox5.Add(self.btn1, proportion=0)
        # hbox5.Add(self.btn2, proportion=1)
        # hbox5.Add(self.btn3, proportion=1)
        # hbox5.Add(self.txt, proportion=1)
        # # self.btnClose = wx.Button(panel, label='', size=(70, 30))
        # # hbox3.Add(self.btnClose, flag=wx.LEFT|wx.BOTTOM, border=5)
        # vbox.Add(hbox5, flag=wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP, border=10)
        # vbox.Add((-1, 50))  # add margin

        # ----------------------------  add log text  ----------------------------------
        hbox4 = wx.BoxSizer(wx.HORIZONTAL)
        self.txtLog = wx.TextCtrl(panel, wx.ID_ANY, '222', style=wx.TE_MULTILINE)
        self.btnSS = wx.Button(panel, label='btn2', size=(70, 30))
        fgs = wx.FlexGridSizer(1, 2, 9, 25)
        fgs.AddMany([(self.txtLog, 1, wx.EXPAND), (self.btnSS, 1, wx.EXPAND)])
        fgs.AddGrowableRow(0, 1)
        fgs.AddGrowableCol(1, 1)
        hbox4.Add(fgs, proportion=1, flag=wx.ALL | wx.EXPAND, border=15)

        #
        # sbox1 = wx.StaticBoxSizer(wx.StaticBox(self, -1, "Basics"), wx.VERTICAL)
        # self.btnStart2 = wx.Button(panel, label='开始升级2', size=(70, 30))
        # self.btnStart3 = wx.Button(panel, label='开始升级3', size=(70, 30))
        # self.btnStart4 = wx.Button(panel, label='开始升级4', size=(70, 30))
        # sbox1.Add(self.btnStart2, 0, wx.ALL, 4)
        # sbox1.Add(self.btnStart3, 0, wx.ALL, 4)
        # sbox1.Add(self.btnStart4, 0, wx.ALL, 4)
        #
        # hbox4.Add(self.txtLog, proportion=1, flag=wx.EXPAND | wx.ALL, border=50)
        # hbox4.Add(self.txtLog2, proportion=1, flag=wx.EXPAND | wx.ALL, border=50)
        # hbox4.Add(sbox1, 0,  wx.EXPAND, 0)
        # vbox.Add(hbox4, flag=wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP, border=10)
        # vbox.Add(fgs, proportion=1, flag=wx.EXPAND, border=10)
        vbox.Add(hbox4, flag=wx.EXPAND, border=10)
        # vbox.Add((-1, 50))  # add margin


        self.statusBar = self.CreateStatusBar()

        panel.SetSizer(vbox)
        self.SetSize((600, 600))
        self.SetTitle('Packaging Tool')
        self.Centre()

    def BindEvent(self):
        # bind the wx.EVT_menu of th menu item to the custom OnQuit() method.
        self.Bind(wx.EVT_MENU, self.OnQuit, self.fitem)
        # bind Close button to Quit event
        # self.Bind(wx.EVT_BUTTON, self.OnQuit, self.btnClose)
        # bind Start button to  OnStart event handler
        self.Bind(wx.EVT_BUTTON, self.OnStart, self.btnStart)

    def OnQuit(self, e):
        self.Close()

    def OnStart(self, e):
        print 'Start'
        self.statusBar.SetStatusText('Start pack')
        fileNameResource = self.m_filePicker1.GetPath()
        if fileNameResource == '':
            self.statusBar.SetStatusText('Please choose a file')
            return


        indexDot = fileNameResource.find('.', 0)
        pathResult = fileNameResource[0:indexDot]
        fileNameResult = pathResult + '.zr'
        print fileNameResult

        obj_Convert = GbFileConvert()
        obj_Convert.hex_bin(fileNameResource, fileNameResult)

        # with open(fileNameResource, 'rb') as fileHandleerResource:
        #     with open(fileNameResult, 'wb') as fileHandlerResult:
        #         # data = fileHandleerResource.read (1)
        #         data = fileHandleerResource.readline()
        #         oEncrypt = GbEncrypt()
        #
        #         dataResult = oEncrypt.EncryptData(data)
        #         print dataResult
        #         fileHandlerResult.write(dataResult)
        #
        #         fileHandlerResult.close()
        #         fileHandleerResource.close()

        # set notify
        self.statusBar.SetStatusText('Pack Finished: ' + fileNameResult)

    def ShowMessage(self):
        wx.MessageBox('Download completed', 'Info', wx.OK | wx.ICON_INFORMATION)


def main():
    ex = wx.App()
    GbFrame(None)
    ex.MainLoop()

if __name__ == '__main__':
    main()
